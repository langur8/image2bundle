var galleryForm = {
	init: function (uploadPath, showPath) {
		$.extend($.fn.fileinput.defaults, imageInputDefaults);

		$("#image_file").fileinput({
			uploadUrl: uploadPath, // server upload action
			uploadAsync: false,
			allowedFileExtensions: ['jpg', 'jpeg', 'png', 'gif'],
			language: 'cz'
		});

		$('#image_file').on('filebatchuploadcomplete', function (event, files, extra) {

			var handler = new AjaxHandler;
			handler.start();
			handler.postSuccess = function (data) {
				$('#image_file').fileinput('reset');
			};
			return $.ajax({
				type: 'post',
				url: showPath})
					.done(function (data) {
						handler.success({response: data});
						handler.stop();
					})
					.fail(function (jqXHR, textStatus, errorThrown) {
						handler.stop();
					});
		});

		$('.fileinput-remove').on('click', function () {
			$('#image_file').fileinput('reset');
		});
	}
}

if(typeof imageInputDefaults == 'undefined') {
    var imageInputDefaults = {
        language: 'en',
        showCaption: true,
        showPreview: true,
        showRemove: true,
        showUpload: true,
        showCancel: true,
        showUploadedThumbs: false,
        mainClass: '',
        previewClass: '',
        captionClass: '',
        mainTemplate: null,
        initialCaption: '',
        initialPreview: [],
        initialPreviewDelimiter: '*$$*',
        initialPreviewConfig: [],
        initialPreviewThumbTags: [],
        previewThumbTags: {},
        initialPreviewShowDelete: true,
        deleteUrl: '',
        deleteExtraData: {},
        overwriteInitial: false,
        layoutTemplates: {footer: ''},
//	previewTemplates: defaultPreviewTemplates,
//	allowedPreviewTypes: defaultPreviewTypes,
        allowedPreviewMimeTypes: null,
        allowedFileTypes: null,
        allowedFileExtensions: null,
        customLayoutTags: {},
        customPreviewTags: {},
//	previewSettings: defaultPreviewSettings,
//	fileTypeSettings: defaultFileTypeSettings,
        previewFileIcon: '<i class="glyphicon glyphicon-file"></i>',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i> &nbsp;',
        browseClass: 'btn btn-primary',
        removeIcon: '<i class="glyphicon glyphicon-trash"></i> ',
        removeClass: 'btn btn-default',
        cancelIcon: '<i class="glyphicon glyphicon-ban-circle"></i> ',
        cancelClass: 'btn btn-default',
        uploadIcon: '<i class="glyphicon glyphicon-upload"></i> ',
        uploadClass: 'btn btn-default',
        uploadUrl: null,
        uploadAsync: true,
        uploadExtraData: {},
        maxFileSize: 2048,
        minFileCount: 1,
        maxFileCount: 5,
        msgValidationErrorClass: 'text-danger',
        msgValidationErrorIcon: '<i class="glyphicon glyphicon-exclamation-sign"></i> ',
        msgErrorClass: 'file-error-message',
        progressClass: "progress-bar progress-bar-warning progress-bar-striped active",
        progressCompleteClass: "progress-bar progress-bar-success",
        previewFileType: 'image',
        wrapTextLength: 250,
        wrapIndicator: ' <span class="wrap-indicator" title="{title}" onclick="{dialog}">[&hellip;]</span>',
        elCaptionContainer: null,
        elCaptionText: null,
        elPreviewContainer: null,
        elPreviewImage: null,
        elPreviewStatus: null,
        elErrorContainer: null,
        slugCallback: null,
        dropZoneEnabled: true,
        dropZoneTitleClass: 'file-drop-zone-title',
        fileActionSettings: {},
        otherActionButtons: '',
        textEncoding: 'UTF-8',
        ajaxSettings: {},
        ajaxDeleteSettings: {},
        showAjaxErrorDetails: true
    }
}