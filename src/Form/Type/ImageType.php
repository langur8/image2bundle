<?php

namespace dlouhy\ImageBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use dlouhy\ImageBundle\Entity\Image;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ImageType extends AbstractType
{


	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
				->add('description', TextType::class, [
					'label' => 'image.description',
				])
				->add('position', IntegerType::class, [
					'label' => 'image.priority',
		]);
	}


	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => Image::class
		]);
	}

}
