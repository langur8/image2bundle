<?php

namespace dlouhy\ImageBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use dlouhy\ImageBundle\Entity\ImageGallery;
use dlouhy\ImageBundle\Entity\Image;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ImageGalleryType extends AbstractType
{

	/**
	 * @var ImageGallery
	 */
	protected $entity;


	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{

		$entity = $options['data'];

		$builder->add('images', CollectionType::class, [
			'entry_type' => ImageType::class,
			'label' => false,
			'allow_add' => false,
			'allow_delete' => false,
			'by_reference' => false
		]);

		if ($entity->getId()) {
			$builder->add('homeImage', EntityType::class, [
				'class' => Image::class,
				'expanded' => true,
				'multiple' => false,
				'choice_label' => function() {
					return 'gallery.homeImage';
				},
				'label' => false,
				'query_builder' => function(EntityRepository $er) use ($entity) {
					return $er->createQueryBuilder('i')->where('i.imageGallery = ?1')->andWhere('i.active = ?2')->setParameters(array(1 => $entity, 2 => 1));
				}
			]);
		}

		if ($entity->getPresentImages()->count() > 0) {
			$builder->add('save', SubmitType::class, [
				'label' => 'gallery.save'
			]);
		}
	}


	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'required' => false,
			'data_class' => ImageGallery::class
		]);
	}

}
