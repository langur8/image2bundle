<?php

namespace dlouhy\ImageBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use dlouhy\ImageBundle\Entity\ImageGallery;
use dlouhy\ImageBundle\Entity\Image;
use dlouhy\ImageBundle\Form\Type\ImageGalleryType;
use Symfony\Component\HttpFoundation\File\UploadedFile;

trait BaseGalleryControllerTrait
{

	/**
	 * Nazev entity ke ktere je galerie ulozena
	 *
	 * @var string
	 */
	protected $sParentEntity;
	
	/**
	 * Nazev promenne entity sParentEntity, ke ktere je galerie ulozena
	 *
	 * @var string
	 */	
	protected $sParentProperty;
	
	
	/**
	 * Getter pro galerii k predkovi
	 *
	 * @var string
	 */	
	private $sParentGetter;
	
	/**
	 * Setter pro galerii k predkovi
	 *
	 * @var string
	 */		
	private $sParentSetter;
	
	/**
	 * Nazev editacniho formulare
	 *
	 * @var string
	 */
	protected $sForm;	
	
	/**
	 * Nazev entity balicku kratky
	 *
	 * @var string
	 */
	protected $sBundle;

	/**
	 * Nazev controlleru
	 *
	 * @var string
	 */
	protected $sController;

	/**
	 * Nazev akce
	 *
	 * @var string
	 */
	protected $sAction;

	/**
	 * Umisteni sablony s dvojteckovou notaci
	 *
	 * @var string
	 */
	protected $sTemplate;

	/**
	 * Umisteni sablony formulare s dvojteckovou notaci
	 *
	 * @var string
	 */
	protected $sFormTemplate;	
	
	/**
	 * Presmerovani
	 * 
	 * @var string
	 */
	protected $redirect;
	
	/**
	 * Entita, k niz je vazana galerie
	 */
	protected $parent;	
	
	/**
	 * instance Galerie, vetsinou ImageGallery
	 * @var ImageGallery
	 */
	protected $gallery;
	
	/**
	 * @var bool
	 */
	private $initialized = false;


	/**
	 *
	 * @param Request $request
	 * @param int $id
	 * @return JsonResponse|Response
	 */
	public function baseGalleryAction(Request $request, $id)
	{		
		$this->init($request, $id);
			
		if($this->redirect === null) {
			$this->redirect = $this->generateUrl($request->get('_route').'_save', ['id' => $id]);
		}	
		
		$form = $this->getForm($this->gallery, $this->redirect);
		
		if($request->isXmlHttpRequest()) {
			return new JsonResponse([
				'replace' => true,
				'elementId' => 'gallery',
				'html' => $this->renderView($this->sFormTemplate, [
					'parent' => $this->parent,
					'form' => $form->createView(),
					'deleteRoute' => $request->get('_route').'_delete',
					'errors' => []
				])], 200);
		}
				
		return $this->render($this->sTemplate, [
			'form' => $form->createView(),
			'parent' => $this->parent,
			'uploadPath' => $this->generateUrl($request->get('_route').'_upload', ['id' => $id]),
			'deleteRoute' => $request->get('_route').'_delete',
			'showPath' => $this->generateUrl($request->get('_route'), ['id' => $id])
			]);

	}


	/**
	 * @param Request $request
	 * @param int $id
	 * @return JsonResponse
	 */
	public function baseUploadImages(Request $request, $id)
	{		
		$this->init($request, $id);
		$images = $request->files->get('gallery');
		if (empty($images) || !isset($images['image']['file'])) {
			return new JsonResponse(['msg' => 'No images submited'], 400);
		}
		
		call_user_func(array($this->parent, $this->sParentSetter), $this->gallery);
		$this->save($this->parent);		
		
		foreach ($images['image']['file'] as $file) {
			if (!$file instanceof UploadedFile) {
				return new JsonResponse(['Image not included'], 400);
			}
			$this->get('dlouhy_image.image_uploader')->upload($file, $this->gallery);
		}

		return new JsonResponse(['msg' => 'OK'], 200);
	}
	

	/**
	 * @param Request $request
	 * @param int $id
	 * @return JsonResponse
	 */
	public function baseSaveGalleryAction(Request $request, $id)
	{
		$this->init($request, $id);
				
		$form = $this->getForm($this->gallery, $this->redirect);
		$form->handleRequest($request);	

		$message = '';
		$returnCode = 400;		
		if ($form->isValid()) {
			$this->gallery->setDefaults();				
			$this->save($this->gallery);
			$form = $this->getForm($this->gallery, $this->redirect);
			$returnCode = 200;
		}
		
		$addRedirect = [];
		if($this->redirect !== null) {
			$addRedirect['redirect'] = $this->redirect;
		}

		return new JsonResponse([
			'replace' => true,
			'message' => $message,
			'html' => $this->renderView($this->sFormTemplate, [
				'parent' => $this->parent,
				'form' => $form->createView(),
				'deleteRoute' => str_replace('_save', '_delete', $request->get('_route')),
				'errors' => []
			])] + $addRedirect, $returnCode);
	}		


	/**
	 *
	 * @param Request $request
	 * @param int $id
	 * @param int $imageId
	 * @return JsonResponse
	 * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
	 */
	public function baseDeleteImage(Request $request, $id, $imageId)
	{	
		$this->init($request, $id);
		
		if (!$imageId) {
			throw $this->createNotFoundException('Bad parameter id');
		}

		$repoImg = $this->getDoctrine()->getRepository(Image::class);
		$image = $repoImg->find($imageId);

		if (!$image instanceof Image) {
			throw $this->createNotFoundException('The Image does not exist');
		}
		
		$this->get('dlouhy_image.image_uploader')->delete($image);
		$form = $this->getForm($this->gallery, $this->redirect);

		return new JsonResponse([
			'replace' => true,
			'elementId' => 'gallery',
			'html' => $this->renderView($this->sFormTemplate, [
				'parent' => $this->parent,
				'form' => $form->createView(),
				'deleteRoute' => $request->get('_route'),
				'errors' => []
			])], 200);
	}		
				

	/**
	 * @param Request $request
	 * @param int $id
	 * @return void
	 * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
	 */
	protected function init(Request $request, $id)
	{
		if($this->initialized === true) {
			return;
		}
		
		$matches = array();
		$controller = $request->attributes->get('_controller');
		preg_match('/(.*)\\\Controller\\\(.*)Controller::(.*)Action/', $controller, $matches);				

		if ($this->sBundle === null) {
			$this->sBundle = str_replace("\\", '', $matches[1]);
		}
		if ($this->sParentEntity === null) {
			$this->sParentEntity = $matches[1] . '\Entity\\' . $matches[2];
		}
		if ($this->sParentProperty === null) {
			$this->sParentProperty = 'ImageGallery';
		}		
		if ($this->sParentGetter === null) {
			$this->sParentGetter = 'get'.$this->sParentProperty;
		}
		if ($this->sParentSetter === null) {
			$this->sParentSetter = 'set'.$this->sParentProperty;
		}		
		if ($this->sForm === null) {
			$this->sForm = ImageGalleryType::class;
		}				
		if ($this->sController === null) {
			$this->sController = $matches[2];
		}
		if ($this->sAction === null) {
			$this->sAction = $matches[3];
		}
		if ($this->sTemplate === null) {
			$this->sTemplate = $this->sBundle . ':' . $this->sController . ':' . $this->sAction . '.html.twig';
		}
		if ($this->sFormTemplate === null) {
			$this->sFormTemplate = $this->sBundle . ':' . $this->sController . ':gallery_form_content.html.twig';
		}		
		
		if (!$id) {
			throw $this->createNotFoundException('Bad parameter id');
		}

		$repo = $this->getDoctrine()->getRepository($this->sParentEntity);
		$this->parent = $repo->find($id);

		if ($this->parent === false) {
			throw $this->createNotFoundException('Not found');
		}
		
		$this->gallery = call_user_func([$this->parent, $this->sParentGetter]);

		if (!$this->gallery instanceof ImageGallery) {
			$this->gallery = new ImageGallery;
			$this->gallery->setUniqueFolder();
			call_user_func([$this->parent, $this->sParentSetter], $this->gallery);
		}
		
		$this->initialized = true;
	}
	

	/**
	 * @param string $entity
	 * @param string $action
	 * @param array $options
	 * @return \Symfony\Component\Form\Form
	 */
	protected function getForm($entity, $action, $options = array())
	{
		return $this->createForm($this->sForm, $entity, array('action' => $action) + $options);
	}	
	

	/**
	 * @param \dlouhy\ImageBundle\Entity\EntityAbstract $entity
	 */
	protected function save($entity)
	{
		$em = $this->getDoctrine()->getManager();
		$em->persist($entity);
		$em->flush();
	}
	
		
}
