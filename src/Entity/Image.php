<?php

/**
 * Entita obrazku
 *
 * @author Vaclav Dlouhy <vdlouhy@atlas.cz>
 */

namespace dlouhy\ImageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use dlouhy\UtilsBundle\Utils\Strings;

/**
 * @ORM\Entity(repositoryClass="dlouhy\ImageBundle\Entity\Repository\ImageRepository")
 * @ORM\Table(name="img__images", indexes={
 * 		@ORM\Index(name="idx_search", columns={"filename", "suffix"})
 * 		})
 * @ORM\HasLifecycleCallbacks()
 *
 */
class Image extends EntityAbstract
{

	/**
	 * Relative path to web dir
	 */
	const WEB_DIR = '/../../../../../web/';

	/**
	 * Image upload dir in web dir
	 */
	const UPLOAD_DIR = 'uploads/images';

	/**
	 * @var int
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	/**
	 * @var \DateTime
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $modified;

	/**
	 * @var bool
	 * @ORM\Column(type="boolean")
	 */
	protected $active = true;

	/**
	 * @var string
	 * @ORM\Column(type="string", length=90, nullable=true)
	 */
	protected $name;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $filename;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $suffix;

	/**
	 * @var int
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $position = 1;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $description;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $sha1;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $url;

	/**
	 * @var string
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $mime;

	/**
	 * @var string
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $size;

	/**
	 * @var UploadedFile
	 * @Assert\File(maxSize="6000000")
	 */
	private $file;

	/**
	 * @var ImageGallery
	 * @ORM\ManyToOne(targetEntity="ImageGallery", inversedBy="images")
	 * @ORM\JoinColumn(name="image_gallery_id", referencedColumnName="id", onDelete="CASCADE")
	 * */
	protected $imageGallery;


	/**
	 * @ORM\PreUpdate
	 */
	public function preUpdate()
	{
		$this->modified = new \DateTime;
	}


	/**
	 * @ORM\PrePersist
	 */
	public function prePersist()
	{
		$this->created = new \DateTime;
	}


	/**
	 * @ORM\PostPersist()
	 * @ORM\PostUpdate()
	 */
	public function upload()
	{
		if (null === $this->getFile()) {
			return;
		}

		// if there is an error when moving the file, an exception will
		// be automatically thrown by move(). This will properly prevent
		// the entity from being persisted to the database on error
		$this->getFile()->move($this->getUploadRootDir(), $this->getPath());

		// check if we have an old image
		if (isset($this->temp)) {
			// delete the old image
			unlink($this->getUploadRootDir() . '/' . $this->temp);
			// clear the temp image path
			$this->temp = null;
		}
		$this->file = null;
	}


	/**
	 * @ORM\PostRemove()
	 */
	public function removeUpload()
	{
		$file = str_replace('__deleted__', '', $this->getAbsolutePath());
		if (file_exists($file)) {
			unlink($file);
		}
	}


	/**
	 * @param string $filter
	 * @return string
	 */
	public function getAbsolutePath($filter = null)
	{
		return null === $this->filename ? null : $this->getUploadRootDir() . '/' . $this->getPath($filter);
	}


	/**
	 * @param string $filter
	 * @return string
	 */
	public function getWebPath($filter = null)
	{
		return null === $this->filename ? null : '/' . $this->getUploadDir() . '/' . $this->getImageGallery()->getFolder() . '/' . $this->getPath($filter);
	}


	/**
	 * @return string
	 */
	public function getUploadRootDir()
	{
		// the absolute directory path where uploaded
		// documents should be saved
		return __DIR__ . static::WEB_DIR . $this->getUploadDir() . '/' . $this->getImageGallery()->getFolder();
	}


	/**
	 * @param string $filter
	 * @return string
	 */
	public function getPath($filter = null)
	{
		return $this->filename . ($filter ? '-' . $filter : '' ) . '.' . $this->suffix;
	}


	/**
	 * @return string
	 */
	protected function getUploadDir()
	{
		// get rid of the __DIR__ so it doesn't screw up
		return static::UPLOAD_DIR;
	}


	/**
	 *
	 */
	private function generateFilename()
	{
		$basename = Strings::Slug(preg_replace('/\\.[^.\\s]{3,4}$/', '', $this->getFile()->getClientOriginalName()));
		$suffix = $this->getFile()->guessExtension();

		$i = 1;
		$filename = $basename;
		while (file_exists($this->getUploadRootDir() . '/' . $filename . '.' . $suffix)) {
			$filename = $basename . '-' . $i;
			$i++;
		}
		$this->setFilename($filename);
		$this->setSuffix($suffix);
	}


	/**
	 * Sets file.
	 *
	 * @param UploadedFile $file
	 */
	public function setFile(UploadedFile $file = null)
	{
		$this->file = $file;
		// check if we have an old image path
		if (isset($this->filename)) {
			// store the old name to delete after the update
			$this->temp = $this->filename;
			$this->filename = null;
		} else {
			$this->filename = 'initial';
		}
	}


	/**
	 * Get file.
	 *
	 * @return UploadedFile
	 */
	public function getFile()
	{
		return $this->file;
	}


	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	 */
	public function preUpload()
	{
		if (null !== $this->getFile()) {
			$this->generateFilename();
			$this->setSha1(sha1_file($this->getFile()->getPathname()));
			$this->setMime($this->getFile()->getMimeType());
			$this->setSize($this->getFile()->getSize());
		}
	}


	/**
	 * @return string
	 */
	public function getFilenameWithSuffix()
	{
		return $this->getFilename() . '.' . $this->getSuffix();
	}


//GENERATED

	public function getId()
	{
		return $this->id;
	}


	public function getCreated()
	{
		return $this->created;
	}


	public function getModified()
	{
		return $this->modified;
	}


	public function getActive()
	{
		return $this->active;
	}


	public function getName()
	{
		return $this->name;
	}


	public function getFilename()
	{
		return $this->filename;
	}


	public function getSuffix()
	{
		return $this->suffix;
	}


	public function getPosition()
	{
		return $this->position;
	}


	public function getDescription()
	{
		return $this->description;
	}


	public function getSha1()
	{
		return $this->sha1;
	}


	public function getUrl()
	{
		return $this->url;
	}


	public function getMime()
	{
		return $this->mime;
	}


	public function getSize()
	{
		return $this->size;
	}


	public function getImageGallery()
	{
		return $this->imageGallery;
	}


	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}


	public function setCreated(\DateTime $created)
	{
		$this->created = $created;
		return $this;
	}


	public function setModified(\DateTime $modified)
	{
		$this->modified = $modified;
		return $this;
	}


	public function setActive($active)
	{
		$this->active = $active;
		return $this;
	}


	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}


	public function setFilename($filename)
	{
		$this->filename = $filename;
		return $this;
	}


	public function setSuffix($suffix)
	{
		$this->suffix = $suffix;
		return $this;
	}


	public function setPosition($position)
	{
		$this->position = $position;
		return $this;
	}


	public function setDescription($description)
	{
		$this->description = $description;
		return $this;
	}


	public function setSha1($sha1)
	{
		$this->sha1 = $sha1;
		return $this;
	}


	public function setUrl($url)
	{
		$this->url = $url;
		return $this;
	}


	public function setMime($mime)
	{
		$this->mime = $mime;
		return $this;
	}


	public function setSize($size)
	{
		$this->size = $size;
		return $this;
	}


	public function setImageGallery(ImageGallery $imageGallery)
	{
		$this->imageGallery = $imageGallery;
		return $this;
	}

}
